
var term = new Terminal();
term.open(document.getElementById('factory-test-terminal'),false);
term.resize(100, 100); 

const tests = ["api/v1/platform/test"]

writeHeader = function() {
	term.writeln([
		'VOXL Portal Factory Test',
		''
	].join('\n\r'));
}

factoryTestWriteln = function(str){
	term.writeln(str);
}
factoryTestWritelnPass = function(str){
	term.writeln('\x1b[32m' + str + '\x1b[0m');
}
factoryTestWritelnFail = function(str){
	term.writeln('\x1b[31;1m' + str + '\x1b[0m');
}

factoryTestPrintResults = function(data){
	console.log(data)
	
	if(data['factoryMode']){
		factoryTestWriteln('<< SDK: factoryMode: ' + data['factoryMode'])
		//var result = JSON.stringify(data);
		//factoryTestWriteln(result);

		factoryTestWriteln("")
		var compute = data['compute'];
		
		// show low level data
		factoryTestWriteln('---- voxl-camera-server-----------------------------------')
		if(compute){
			factoryTestWriteln(' < platform: imageSensor')
			var result0 = data['compute']['imageSensor0']['result']
			if(result0 == true){
				factoryTestWritelnPass('     imageSensor0: ' + compute['imageSensor0']['probe'])
			} else {
				factoryTestWritelnFail(' image sensor failure')
				factoryTestWritelnFail('     imageSensor0: ' + compute['imageSensor0']['probe'])
			}
			var result1 = data['compute']['imageSensor1']['result']
			if(result1 == true){
				factoryTestWritelnPass('     imageSensor1: ' + compute['imageSensor1']['probe'])
			} else {
				factoryTestWritelnFail(' image sensor failure')
				factoryTestWritelnFail('     imageSensor1: ' + compute['imageSensor1']['probe'])
			}
		} else {
			factoryTestWritelnFail('FATAL ERROR: unable to get compute info')
		}

		// show high level data
		factoryTestWriteln("")
		var voxlCameraServer = data['voxl-camera-server'];
		if(voxlCameraServer){
			factoryTestWriteln(' < SDK: voxl-camera-server: ')
			var result = data['voxl-camera-server']['result']
			if(result){
				factoryTestWritelnPass('           : mpaStereo: ' + voxlCameraServer['mpaStereo'])
			} else {
				factoryTestWritelnFail('           : mpaStereo: ' + voxlCameraServer['mpaStereo'])
			}
		}
		else {
			factoryTestWritelnFail('FATAL ERROR: unable to get voxl-camera-server info')
		}

		factoryTestWriteln("")
		factoryTestWriteln('---- voxl-px4 ---------------------------------------------')
		var voxlPx4 = data['voxl-px4'];
		if(voxlPx4){
			factoryTestWriteln(' < SDK: voxl-px4: ')
			if(voxlPx4['execStart']){
				var execStartStr = voxlPx4['execStart'];
				factoryTestWriteln(' ExecStart' + execStartStr)
			}

			if(voxlPx4["error"] == true){
				factoryTestWritelnFail('ERROR: voxl-px4: likely not running?')
			}
			else {
				if(voxlPx4['sensor_accel']){
					var sensor = voxlPx4['sensor_accel']
					var result = sensor["result"]
					if(result == true) {
						factoryTestWritelnPass('           : accel x,y,z: ' + sensor['accel_x'] + ', ' + sensor['accel_y'] + ',  ' + sensor['accel_z'])
					} else {
						factoryTestWritelnFail('           : accel x,y,z: ' + sensor['accel_x'] + ', ' + sensor['accel_y'] + ',  ' + sensor['accel_z'])
					}
				}
				if(voxlPx4['sensor_gyro']){
					var sensor = voxlPx4['sensor_gyro']
					var result = sensor["result"]
					if(result == true) {
						factoryTestWritelnPass('           : gyro x,y,z ' + sensor['gyro_x'] + ', ' + sensor['gyro_y'] + ', ' + sensor['gyro_z'])
					} else {
						factoryTestWritelnFail('           : gyro x,y,z ' + sensor['gyro_x'] + ', ' + sensor['gyro_y'] + ', ' + sensor['gyro_z'])
					}
				}
				if(voxlPx4['sensor_baro']){
					var sensor = voxlPx4['sensor_baro']
					var result = sensor["result"]
					if(result == true) {
						factoryTestWritelnPass('           : baro temp,pres ' + sensor['temperature'] + ', ' + sensor['pressure'])
					} else {
						factoryTestWritelnFail('           : baro temp,pres ' + sensor['temperature'] + ', ' + sensor['pressure'])
					}
				}
				if(voxlPx4['sensor_mag']){
					var sensor = voxlPx4['sensor_mag']
					var result = sensor["result"]
					if(result == true) {
						factoryTestWritelnPass('           : mag x,y,z ' + sensor['mag_x'] + ', ' + sensor['mag_y'] + ', ' + sensor['mag_z'])
					} else {
						factoryTestWritelnFail('           : mag x,y,z ' + sensor['mag_x'] + ', ' + sensor['mag_y'] + ', ' + sensor['mag_z'])
					}
				}
				if(voxlPx4['sensor_gps']){
					var sensor = voxlPx4['sensor_gps']
					var result = sensor["result"]
					if(result == true) {
						factoryTestWritelnPass('           : sats: ' + sensor['sats'])
					} else {
						factoryTestWritelnFail('           : sats: ' + sensor['sats'])
					}
				}
				if(voxlPx4['battery_status']){
					var sensor = voxlPx4['battery_status']
					var result = sensor["result"]
					if(result == true) {
						factoryTestWritelnPass('           : voltage: ' + sensor['voltage'])
					} else {
						factoryTestWritelnFail('           : voltage: ' + sensor['voltage'])
					}
				}
				if(voxlPx4['input_rc']){
					var sensor = voxlPx4['input_rc']
					var result = sensor["result"]
					if(result == true) {
						factoryTestWritelnPass('           : chancount: ' + sensor['chancount'])
					} else {
						factoryTestWritelnFail('           : chancount: ' + sensor['chancount'])
					}
				}
			}
		}

		factoryTestWriteln("")
		factoryTestWriteln('---- voxl-lepton-server -----------------------------------')
		// show low level
		if(compute){
			factoryTestWriteln(' < platform: compute: i2c0')
			var i2c0 = data['compute']['i2c0']
			if(i2c0){
				if(i2c0["result"] == true){
					factoryTestWritelnPass('            probe: ' + i2c0["probe"])
				} else {
					factoryTestWritelnFail(' failed to probe device on i2c0')
				}
			}
		} else {
			factoryTestWritelnFail('FATAL ERROR: unable to get compute i2c0 info')
		}

		// show high level
		factoryTestWriteln("")
		factoryTestWriteln(' < SDK: voxl-lepton-server: ')
		var voxlFlirServer = data['voxl-lepton-server'];
		if(voxlFlirServer){
			var result = voxlFlirServer['result']
			if(result){
				factoryTestWritelnPass('           : running: ' + voxlFlirServer['running'])
				factoryTestWritelnPass('           : intrinsics: ' + voxlFlirServer['intrinsics'])
				factoryTestWritelnPass('           : mpaColor: ' + voxlFlirServer['mpaColor'])
				factoryTestWritelnPass('           : mpaRaw: ' + voxlFlirServer['mpaRaw'])
			}
			else {
				factoryTestWritelnFail('ERROR: voxl-lepton-server')
				factoryTestWritelnFail('           : intrinsics: ' + voxlFlirServer['intrinsics'])
				factoryTestWritelnFail('           : mpaColor: ' + voxlFlirServer['mpaColor'])
				factoryTestWritelnFail('           : mpaRaw: ' + voxlFlirServer['mpaRaw'])
			}
		}
		else {
			factoryTestWritelnFail('FATAL ERROR: unable to get voxl-lepton-server info')
		}

		factoryTestWriteln('')
		factoryTestWriteln('---- voxl-mavlink-server --------------------------------------')

		// show high level
		factoryTestWriteln(' < SDK: voxl-mavlink-server: ')
		var voxlMavlinkServer = data['voxl-mavlink-server'];
		if(voxlMavlinkServer){
			if(voxlMavlinkServer['result']){
				factoryTestWritelnPass('           : running: ' + voxlMavlinkServer['running'])
				//factoryTestWritelnPass('           : mpaPipe: ' + voxlMavlinkServer['mpaPipe'])
			} else {
				factoryTestWritelnFail('           : running: ' + voxlMavlinkServer['running'])
				//factoryTestWritelnFail('           : mpaPipe: ' + voxlMavlinkServer['mpaPipe'])
			}
		}
		else {
			factoryTestWritelnFail('FATAL ERROR: unable to get voxl-mavlink-server info')
		}

		factoryTestWriteln('')
		factoryTestWriteln('---- voxl-vision-hub --------------------------------------')

		// show high level
		factoryTestWriteln(' < SDK: voxl-vision-hub: ')
		var voxlVisionHub = data['voxl-vision-hub'];
		if(voxlVisionHub){
			if(voxlVisionHub['result']){
				factoryTestWritelnPass('           : running: ' + voxlVisionHub['running'])
				//factoryTestWritelnPass('           : mpaPipe: ' + voxlVisionHub['mpaPipe'])
			} else {
				factoryTestWritelnFail('           : running: ' + voxlVisionHub['running'])
				//factoryTestWritelnFail('           : mpaPipe: ' + voxlVisionHub['mpaPipe'])
			}
		}
		else {
			factoryTestWritelnFail('FATAL ERROR: unable to get voxl-vision-hub info')
		}
		

		factoryTestWriteln('')
		factoryTestWriteln('---- voxl-flow-server -------------------------------------')

		// show high level
		var voxlFlowServer = data['voxl-flow-server'];
		factoryTestWriteln(' < SDK: voxl-flow-server: ')
		if(voxlFlowServer){
			var result = voxlFlowServer['result'];
			console.log("test " + result)
			if(result == true){
				factoryTestWritelnPass('           : running: ' + voxlFlowServer['running'])
				factoryTestWritelnPass('           : mpaPipe: ' + voxlFlowServer['mpaPipe'])
			} else {
				factoryTestWritelnFail('           : running: ' + voxlFlowServer['running'])
				factoryTestWritelnFail('           : mpaPipe: ' + voxlFlowServer['mpaPipe'])
			}
		}
		else {
			factoryTestWritelnFail('FATAL ERROR: unable to getvoxl-flow-server info')
		}

		factoryTestWriteln('')
		factoryTestWriteln('---- voxl-feature-tracker ---------------------------------')

		// show high level
		var voxlFeatureTracker = data['voxl-feature-tracker'];
		factoryTestWriteln(' < SDK: voxl-feature-tracker: ')
		if(voxlFeatureTracker){
			if(voxlFeatureTracker['result']){
				factoryTestWritelnPass('           : running: ' + voxlFeatureTracker['running'])
				factoryTestWritelnPass('           : mpaPipe: ' + voxlFeatureTracker['mpaPipe'])
			} else {
				factoryTestWritelnFail('           : running: ' + voxlFeatureTracker['running'])
				factoryTestWritelnFail('           : mpaPipe: ' + voxlFeatureTracker['mpaPipe'])
			}
		}
		else {
			factoryTestWritelnFail('FATAL ERROR: unable to get voxl-feature-tracker info')
		}
		
		factoryTestWriteln('')
		factoryTestWriteln('---- voxl-uvc-server --------------------------------------')

		// show high level
		var voxlUvcServer = data['voxl-uvc-server'];
		factoryTestWriteln(' < SDK: voxl-uvc-server: ')
		if(voxlUvcServer){
			if(voxlUvcServer['result']){
				factoryTestWritelnPass('           : running: ' + voxlUvcServer['running'])
			} else {
				factoryTestWritelnFail('           : running: ' + voxlUvcServer['running'])
			}
		}
		else {
			factoryTestWritelnFail('FATAL ERROR: unable to get voxl-uvc-server info')
		}

	}

}

writeHeader();

factoryTests = function(factoryMode) {
	factoryTestWriteln("")
	factoryTestWriteln('>> Starting factory tests for ' + factoryMode);

	$.ajax({
		url: 'api/v1/platform/factory-test',
		headers: { 'Content-Type': 'application/json' },
		success : function (data) {
			factoryTestPrintResults(data);
		},
		fail : function(xhr,status,error){
			factoryTestWriteln('XX ERROR: ' + error);
			factoryTestWriteln('XX status: ' + status);
		}
	});
}


$(document).ready(function() { 
	factoryTestWriteln("")
	factoryTestWriteln('Detecting platform type:');
	factoryTestWriteln('>> api/v1/platform/status.json');
	
	$.ajax({
		url: 'api/v1/platform/status.json',
		headers: { 'Content-Type': 'application/json' },
		success : function (data) {
			var result = JSON.stringify(data);

			if(data['factory-mode']){
				factoryTestWriteln('<< ' + data['factory-mode']);
				factoryTests(data['factory-mode'])
			}
			else {
				factoryTestWriteln('XX ERROR:  Unable to get platform status.');
			}
		},
		fail : function(xhr,status,error){
			factoryTestWriteln('XX ERROR: ' + error);
			factoryTestWriteln('XX status: ' + status);
		}
	});
});



term.onKey(e => {
	console.log(e);
	switch (e.key) {
		case '\u0003': // Ctrl+C
			break;

		case '\r': // Enter
			command = '';
			break;

		case '\u007F': // Backspace (DEL)
			break;

		default: // Print all other characters for demo
			term.write(e);
			break;
	}
});
