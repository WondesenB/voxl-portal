#include "cmd_group_manager.h"
#include <dirent.h>
#include "api/portal_api.h"

#ifdef __ANDROID__
#warning building for Android
#define CAMERA_CMD "grep -lir camera_image_metadata_t /data/data/com.modalai.sensors.server/mpa/*/info | sed -e s-/data/data/com.modalai.sensors.server/mpa/--g -e s-/info--g"
#define POSE_CMD "grep -lir pose_vel_6dof_t /data/data/com.modalai.sensors.server/mpa/*/info | sed -e s-/data/data/com.modalai.sensors.server/mpa/--g -e s-/info--g"
#define PTCLOUD_CMD "grep -lir point_cloud_metadata_t /data/data/com.modalai.sensors.server/*/info | sed -e s-/data/data/com.modalai.sensors.server/mpa/--g -e s-/info--g"
#else
#define CAMERA_CMD "grep -lir camera_image_metadata_t /run/mpa/*/info | sed -e s-/run/mpa/--g -e s-/info--g"
#define POSE_CMD "grep -lir pose_vel_6dof_t /run/mpa/*/info | sed -e s-/run/mpa/--g -e s-/info--g"
#define PTCLOUD_CMD "grep -lirE '(point_cloud_metadata_t|tof_data_t)' /run/mpa/*/info | sed -e s-/run/mpa/--g -e s-/info--g"
#endif // __ANDROID__

void CmdGroupManagerCallback(struct mg_connection *c, int ev, void *ev_data, void *fn_data)
{
    if (ev == MG_EV_HTTP_MSG)
    {
        struct mg_http_message *hm = (struct mg_http_message *)ev_data;

        if (mg_http_match_uri(hm, CameraCmdClaimString))
        {
            char response[512];
            FILE *fp = popen(CAMERA_CMD, "r");

            if (fp == NULL)
            {
                printf("Failed to run command\n");
                exit(1);
            }

            char *current_pos = response;
            int len;

            while (fgets(current_pos, sizeof(response), fp))
            {
                int len = strlen(current_pos);
                current_pos[len - 1] = ' ';
                current_pos = &current_pos[len];
            }

            *current_pos = 0;

            pclose(fp);

            mg_http_reply(c, 200, "", "%s", response);
        }
        else if (mg_http_match_uri(hm, PtcloudCmdClaimString))
        {
            char response[512];
            FILE *fp = popen(PTCLOUD_CMD, "r");

            if (fp == NULL)
            {
                printf("Failed to run command\n");
                exit(1);
            }

            char *current_pos = response;
            int len;

            while (fgets(current_pos, sizeof(response), fp))
            {
                int len = strlen(current_pos);
                current_pos[len - 1] = ' ';
                current_pos = &current_pos[len];
            }

            *current_pos = 0;

            pclose(fp);

            mg_http_reply(c, 200, "", "%s", response);
        }
        else if (mg_http_match_uri(hm, PoseCmdClaimString))
        {
            char response[512];
            FILE *fp = popen(POSE_CMD, "r");

            if (fp == NULL)
            {
                printf("Failed to run command\n");
                exit(1);
            }

            char *current_pos = response;
            int len;

            while (fgets(current_pos, sizeof(response), fp))
            {
                int len = strlen(current_pos);
                current_pos[len - 1] = ' ';
                current_pos = &current_pos[len];
            }

            *current_pos = 0;

            pclose(fp);

            mg_http_reply(c, 200, "", "%s", response);
        }
        else if (mg_http_match_uri(hm, PlatformCmdClaimString))
        {
            #ifdef BUILD_QRB5165 // voxl2
            mg_http_reply(c, 200, "", "QRB");
            #else
            mg_http_reply(c, 200, "", "APQ");
            #endif
        }
        else if (mg_http_match_uri(hm, FactoryCmdClaimString))
        {
            #ifdef BUILD_QRB5165 // voxl2
            if(portal_api_v1_suported())
                 mg_http_reply(c, 200, "", "true");
            else
                 mg_http_reply(c, 200, "", "false");
            #else
            mg_http_reply(c, 200, "", "false");
            #endif
        }
    }
}