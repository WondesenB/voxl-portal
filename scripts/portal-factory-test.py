#!/usr/bin/python3

################################################################################
# Copyright 2023 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

import os
from os import path
import json
import subprocess

# This file is setup via voxl-configure-mpa
FACTORY_MODE = "/data/modalai/factory_mode.txt"
VOXL_PX4_SERVICE_FILE = "/etc/systemd/system/voxl-px4.service"
VOXL_FLIR_SERVER_INTRINSICS_FILE = "/data/modalai/opencv_lepton0_raw_intrinsics.yml"

_debug = False

_d0008_model = {
    "compute" : {
        "imageSensor0" : {
            "probe" : "error",
            "result" : False
        },
        "imageSensor1" : {
            "probe" : "error",
            "result" : False
        },
        "i2c0" : {
            "probe" : "",
            "result" : False
        }
    },
    "voxl-camera-server" : {
        "result" : False,
        "running" : False,
        "mpaStereo" : "error"
    },
    "voxl-mavlink-server" : {
        "result" : False,
        "running" : False,
    },
    "voxl-px4" : {
        "result" : False,
        "running" : False,
        "error" : False,
        "execStart" : "",
        "sensor_accel" : {
            "result" : False,
            "accel_x": 0.0,
            "accel_y": 0.0,
            "accel_z": 0.0
        },
        "sensor_gyro" : {
            "result" : False,
            "gyro_x": 0.0,
            "gyro_y": 0.0,
            "gyro_z": 0.0
        },
        "sensor_baro" : {
            "result" : False,
            "temperature" : 0.0,
            "pressure" : 0.0,
        },
        "sensor_mag" : {
            "result" : False,
            "mag_x" : 0.0,
            "mag_y" : 0.0,
            "mag_z" : 0.0
        },
        "battery_status" : {
            "result" : False,
            "voltage" : 0.0,
        },
        "sensor_gps" : {
            "result" : False,
            "lon" : 0.0,
            "lat" : 0.0,
            "alt" : 0.0,
            "vel" : 0.0,
            "sats" : 0.0
        },
        "input_rc" : {
            "result" : False,
            "chancount" : 0
        }
    },
    "voxl-lepton-server" : {
        "result" : False,
        "running" : False,
        "intrinsics" : "error",
        "mpaColor" : "error",
        "mpaRaw" : "error",
    },
    "voxl-flow-server" : {
        "result" : False,
        "running" : False,
        "mpaPipe" : "",
    },
    "voxl-imu-server" : {
        "result" : False,
        "running" : False
    },
    "voxl-uvc-server" : {
        "result" : False,
        "running" : False,
        "device" : ""
    },
    "voxl-vision-hub" : {
        "result" : False,
        "running" : False
    },
    "voxl-feature-tracker" : {
        "result" : False,
        "running" : False,
        "mpaPipe" : ""
    }
}


#
# Utils
#

def read_file_line(filename):
    """
    reads a line from a file

    :param filename: path of file to read
    :return: tuple, boolean flag and file content
    """ 
    if not os.path.exists(filename):
        return False, None
    
    with open(filename) as f:
        content = f.read().strip()
        return True, content

def get_value_from_msg(msg, variable, status=False):
    data = msg.split("\n")
    for line in data:
        # For when trying to get service status
        if status and variable in line:
            val = line.split('|')
            if val[0].strip(" ") == variable:
                return val
        # For when trying to get data from sensor
        elif variable + ":" in line:
            val = line.split(':')[1]
            return val
    return 0

def get_exec_start(service_file_path):
    with open(service_file_path) as f:
        for line in f:
            if line.startswith("ExecStart="):
                return line.strip()[len("ExecStart="):]
    return None

def is_service_running(service_name):
    cmd = ['systemctl', 'is-active', service_name]
    result = subprocess.run(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    if result.returncode == 0:
        return True
    else:
        return False


#
#   Sensor tests
#

# Take in a model of the device being tested and return True/False for test being performed
def test_accel(resp):
    accel_X_min = -1.0
    accel_X_max = 1.0
    accel_Y_min = -1.0
    accel_Y_max = 1.0
    accel_Z_min = -9.0
    accel_Z_max = -10.6
    accel_X = resp["voxl-px4"]["sensor_accel"]["accel_x"]
    accel_Y = resp["voxl-px4"]["sensor_accel"]["accel_y"]
    accel_Z = resp["voxl-px4"]["sensor_accel"]["accel_z"]


    result = True

    # ACCEL X AXIS
    if accel_X > accel_X_max or accel_X < accel_X_min:
        # print("Accel_x out of bounds")
        result = False

    # ACCEL Y AXIS
    if accel_Y > accel_Y_max or accel_Y < accel_Y_min:
        # print("Accel_y out of bounds")
        result = False

    # ACCEL Z AXIS
    if accel_Z < accel_Z_max or accel_Z > accel_Z_min:
        # print("Accel_z out of bounds")
        result = False

    return result

def test_gyro(resp):
    gyro_X_min = 0.9
    gyro_X_max = 1.1
    gyro_Y_min = 0.9
    gyro_Y_max = 1.1
    gyro_Z_min = 0.9
    gyro_Z_max = 1.1
    gyro_X = resp["voxl-px4"]["sensor_gyro"]["gyro_x"]
    gyro_Y = resp["voxl-px4"]["sensor_gyro"]["gyro_y"]
    gyro_Z = resp["voxl-px4"]["sensor_gyro"]["gyro_z"]
    # print("Gyro")
    # print(gyro_X)
    # print(gyro_Y)
    # print(gyro_Z)
    result = True

    # GYRO X AXIS
    if gyro_X > gyro_X_max or gyro_X < gyro_X_min:
        # print("Gyro_x out of bounds")
        result = False

    # GYRO Y AXIS
    if gyro_Y > gyro_Y_max or gyro_Y < gyro_Y_min:
        # print("Gyro_y out of bounds")
        result = False

    # GYRO Z AXIS
    if gyro_Z > gyro_Z_max or gyro_Z < gyro_Z_min:
        # print("Gyro_z out of bounds")
        result = False

    return result

def test_baro(resp):
    baro_press_min = 900.0
    baro_press_max = 1300.0
    baro_temp_min = 20.0
    baro_temp_max= 50.0
    baro_press = resp["voxl-px4"]["sensor_baro"]["pressure"]/100
    baro_temp = resp["voxl-px4"]["sensor_baro"]["temperature"]

    result = True    

    # Baro pressure check
    if baro_press < baro_press_min or baro_press > baro_press_max:
        # print("Baro pressure out of bounds")
        result = False

    # Baro temperature check
    if baro_temp < baro_temp_min or baro_temp > baro_temp_max:
        # print("Baro temp out of bounds")
        result = False

    return result

def test_mag(resp):
    mag_x_min = -1.25  # Based on run rates
    mag_x_max =  1.25  # Based on run rates
    mag_y_min = -1.25  # Based on run rates
    mag_y_max =  1.25  # Based on run rates
    mag_z_min = -1.25  # Based on run rates
    mag_z_max =  1.25  # Based on run rates
    mag_x = resp["voxl-px4"]["sensor_mag"]["mag_x"]
    mag_y = resp["voxl-px4"]["sensor_mag"]["mag_y"]
    mag_z = resp["voxl-px4"]["sensor_mag"]["mag_z"]

    result = True

    # Mag X axis check
    if mag_x < mag_x_min or mag_x > mag_x_max:
        result = False

    # Mag Y axis check
    if mag_y < mag_y_min or mag_y > mag_y_max:
        result = False

    # Mag Z axis check
    if mag_z < mag_z_min or mag_z > mag_z_max:
        result = False

    return result

def test_gps(resp):
    gps_sats_min = 1
    gps_long = resp["voxl-px4"]["sensor_gps"]["lon"] 
    gps_lat = resp["voxl-px4"]["sensor_gps"]["lat"]
    gps_alt = resp["voxl-px4"]["sensor_gps"]["alt"]
    gps_vel = resp["voxl-px4"]["sensor_gps"]["vel"]
    gps_sats = resp["voxl-px4"]["sensor_gps"]["sats"]

    result = True

    # Check that we are connected to at least 1 sat... will likely fail if we are indoors
    if gps_sats < gps_sats_min:
        result = False

    return result

def test_battery(resp):
    voltage_min = 0
    voltage_max = 25.2
    voltage = resp["voxl-px4"]["battery_status"]["voltage"]

    result = True

    # Check that voltage is in acceptable range (max for D0008 is 25.2V at full battery I believe)
    if voltage <= voltage_min or voltage >= voltage_max:
        result = False

    return result

def test_rc(resp):
    chancount_min = 1
    chancount = resp["voxl-px4"]["input_rc"]["chancount"]

    result = True

    # Check that we are connected to at least one channel (Might need to check for a specific number later?)
    if chancount < chancount_min:
        result = False

    return result

#
#   D0008 Tests
#

def voxl_px4_factory_test(resp, factory_mode):

    resp["voxl-px4"] = {
        "result" : False,
        "error" : False,
        "execStart" : "",
        "sensor_accel" : {
            "result" : False,
            "accel_x": 0.0,
            "accel_y": 0.0,
            "accel_z": 0.0
        },
        "sensor_gyro" : {
            "result" : False,
            "gyro_x": 0.0,
            "gyro_y": 0.0,
            "gyro_z": 0.0
        },
        "sensor_baro" : {
            "result" : False,
            "temperature" : 0.0,
            "pressure" : 0.0,
        },
        "sensor_mag" : {
            "result" : False,
            "mag_x" : 0.0,
            "mag_y" : 0.0,
            "mag_z" : 0.0
        },
        "battery_status" : {
            "result" : False,
            "voltage" : 0.0,
        },
        "sensor_gps" : {
            "result" : False,
            "lon" : 0.0,
            "lat" : 0.0,
            "alt" : 0.0,
            "vel" : 0.0,
            "sats" : 0.0
        },
        "input_rc" : {
            "result" : False,
            "chancount" : 0
        }
    }

    if "D0008" in factory_mode:

        #
        # SDK checks
        #
        if is_service_running("voxl-px4"):
            resp["voxl-px4"]["result"] = True
            resp["voxl-px4"]["running"] = True
            resp["voxl-px4"]["execStart"] = get_exec_start(VOXL_PX4_SERVICE_FILE)

            TOPIC_NAME_POSITION = 3

            listener_command = ['px4-listener', '--instance', '0', '-n', '1']

            get_commander_state = listener_command[:]
            get_accel = listener_command[:]
            get_gyro = listener_command[:]
            get_magnetometer = listener_command[:]
            get_gps = listener_command[:]
            get_barometer = listener_command[:]
            get_battery_status = listener_command[:]
            get_rc_input = listener_command[:]
            
            get_commander_state.insert(TOPIC_NAME_POSITION, 'commander_state')

            command_loop = []
            get_accel.insert(TOPIC_NAME_POSITION, 'sensor_accel')
            command_loop.append(get_accel)
            get_gyro.insert(TOPIC_NAME_POSITION, 'sensor_gyro')
            command_loop.append(get_gyro)
            get_magnetometer.insert(TOPIC_NAME_POSITION, 'sensor_mag')
            command_loop.append(get_magnetometer)
            get_gps.insert(TOPIC_NAME_POSITION, 'sensor_gps')
            command_loop.append(get_gps)
            get_barometer.insert(TOPIC_NAME_POSITION, 'sensor_baro')
            command_loop.append(get_barometer)
            get_battery_status.insert(TOPIC_NAME_POSITION, 'battery_status')
            command_loop.append(get_battery_status)
            get_rc_input.insert(TOPIC_NAME_POSITION, 'input_rc')
            command_loop.append(get_rc_input)

            try:
                for cmd in command_loop:

                    sample = subprocess.check_output(cmd).decode("utf-8")
                    
                    if "timestamp" in sample:
                        if "sensor_accel" in sample:
                            resp["voxl-px4"]["sensor_accel"] = {
                                "accel_x" : float(get_value_from_msg(sample, "x")),
                                "accel_y" : float(get_value_from_msg(sample, "y")),
                                "accel_z" : float(get_value_from_msg(sample, "z"))
                            }
                            resp["voxl-px4"]["sensor_accel"]["result"] = test_accel(resp)
                            if not resp["voxl-px4"]["sensor_accel"]["result"]:
                                resp["voxl-px4"]["result"] = False

                        elif "sensor_gyro" in sample:
                            resp["voxl-px4"]["sensor_gyro"] = {
                                "gyro_x" : float(get_value_from_msg(sample, "x")),
                                "gyro_y" : float(get_value_from_msg(sample, "y")),
                                "gyro_z" : float(get_value_from_msg(sample, "z"))
                            }
                            resp["voxl-px4"]["sensor_gyro"]["result"] = test_gyro(resp)
                            if not resp["voxl-px4"]["sensor_gyro"]["result"]:
                                resp["voxl-px4"]["result"] = False

                        elif "sensor_baro" in sample:
                            resp["voxl-px4"]["sensor_baro"] = {
                                "temperature" : float(get_value_from_msg(sample, "temperature")),
                                "pressure" : float(get_value_from_msg(sample, "pressure"))
                            }
                            resp["voxl-px4"]["sensor_baro"]["result"] = test_baro(resp)
                            if not resp["voxl-px4"]["sensor_baro"]["result"]:
                                resp["voxl-px4"]["result"] = False

                        elif "sensor_mag" in sample:
                            resp["voxl-px4"]["sensor_mag"] = {
                                "mag_x" : float(get_value_from_msg(sample, "x")),
                                "mag_y" : float(get_value_from_msg(sample, "y")),
                                "mag_z" : float(get_value_from_msg(sample, "z"))
                            }
                            resp["voxl-px4"]["sensor_mag"]["result"] = test_mag(resp)
                            if not resp["voxl-px4"]["sensor_mag"]["result"]:
                                resp["voxl-px4"]["result"] = False

                        elif "sensor_gps" in sample:
                            resp["voxl-px4"]["sensor_gps"] = {
                                "lon" : float(get_value_from_msg(sample, "lon")),
                                "lat" : float(get_value_from_msg(sample, "lat")),
                                "alt" : float(get_value_from_msg(sample, "alt")),
                                "vel" : float(get_value_from_msg(sample, "vel_m_s")),
                                "sats" : int(get_value_from_msg(sample, "satellites_used"))
                            }
                            resp["voxl-px4"]["sensor_gps"]["result"] = test_gps(resp)
                            if not resp["voxl-px4"]["sensor_gps"]["result"]:
                                resp["voxl-px4"]["result"] = False

                        elif "battery_status" in sample:
                            resp["voxl-px4"]["battery_status"] = {
                                "voltage" : float(get_value_from_msg(sample, "voltage_filtered_v")),
                            }
                            resp["voxl-px4"]["battery_status"]["result"] = test_battery(resp)
                            if not resp["voxl-px4"]["battery_status"]["result"]:
                                resp["voxl-px4"]["result"] = False

                        elif "input_rc" in sample:
                            resp["voxl-px4"]["input_rc"] = {
                                "chancount" : float(get_value_from_msg(sample, "channel_count"))
                            }
                            resp["voxl-px4"]["input_rc"]["result"] = test_rc(resp)
                            if not resp["voxl-px4"]["input_rc"]["result"]:
                                resp["voxl-px4"]["result"] = False

            except:
                resp["voxl-px4"]["error"] = True
                resp["voxl-px4"]["result"] = False
        else:
            resp["voxl-px4"]["running"] = False
            resp["voxl-px4"]["result"] = False

def voxl_flir_server_test(resp):
    result = True

    #
    # Platform checks
    #

    # Check for I2C device 0x2A on /dev/i2c0
    FLIR_I2C_BUS = 0
    FLIR_I2C_ADDR = "2a"
    # I2C FLIR Lepton on M0130 J8 - /dev/i2c0
    # i2cdetect -a -y -r 0
    p = subprocess.Popen(['i2cdetect', '-a', '-y', '-r', '0'], stdout=subprocess.PIPE)
    output, err = p.communicate()
    output_str = output.decode('utf-8')
    i2c_detect_lines = output_str.split('\n')
    #print(i2c_detect_lines)

    #    0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
    #00: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    #10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    #20: -- -- -- -- -- -- -- -- -- -- 2a -- -- -- -- --
    #30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    #40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    #50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    #60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
    #70: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

    i2c_detected = False
    resp["compute"]["i2c0"] = {}
    resp["compute"]["i2c0"]["result"] = False
    for line in i2c_detect_lines:
        if FLIR_I2C_ADDR in line:
            i2c_detected = True
            resp["compute"]["i2c0"]["result"] = True
            resp["compute"]["i2c0"]["probe"] = FLIR_I2C_ADDR
     
    if not i2c_detected:
        result = False

    #
    # SDK checks
    #
    if is_service_running("voxl-lepton-server"):
        resp["voxl-lepton-server"]["running"] = True

    # Check for intrinsics calibration file
    if path.exists(VOXL_FLIR_SERVER_INTRINSICS_FILE):
        resp["voxl-lepton-server"]["intrinsics"] = VOXL_FLIR_SERVER_INTRINSICS_FILE
    else:
        result = False

    # Check for MPA pipes
    VOXL_FLIR_SERVER_PIPE_COLOR="/run/mpa/lepton0_color/info"
    VOXL_FLIR_SERVER_RAW_COLOR="/run/mpa/lepton0_raw/info"
    if path.exists(VOXL_FLIR_SERVER_PIPE_COLOR):
        resp["voxl-lepton-server"]["mpaColor"] = VOXL_FLIR_SERVER_PIPE_COLOR
    else:
        result = False
    if path.exists(VOXL_FLIR_SERVER_RAW_COLOR):
        resp["voxl-lepton-server"]["mpaRaw"] = VOXL_FLIR_SERVER_RAW_COLOR
    else:
        result = False

    resp["voxl-lepton-server"]["result"] = result

def voxl_camera_server_factory_test(resp, factory_mode, dmesg_lines):

    if "D0008" in factory_mode:
        result = True

        #
        # Platform Checks
        #

        # check that senors were probed
        cam0_success = "CAM_START_DEV Success, sensor_id:0x9281,sensor_slave_addr:0xe2"
        cam1_success = "CAM_START_DEV Success, sensor_id:0x9281,sensor_slave_addr:0xe4"

        for line in dmesg_lines:
            if cam0_success in line:
                resp["compute"]["imageSensor0"]["probe"] = cam0_success
            elif cam1_success in line:
                resp["compute"]["imageSensor1"]["probe"] = cam1_success

        if not "error" in resp["compute"]["imageSensor0"]["probe"]:
            resp["compute"]["imageSensor0"]["result"] = True
        else:
            result = False

        if not "error" in resp["compute"]["imageSensor1"]["probe"]:
            resp["compute"]["imageSensor1"]["result"] = True
        else:
            result = False

        #
        # SDK Checks
        #
        if is_service_running("voxl-camera-server"):
            resp["voxl-camera-server"]["running"] = True
        
        # camera server enumerating as stereo
        VOXL_CAMERA_SERVER_STEREO_PIPE="/run/mpa/stereo/info"
        if path.exists(VOXL_CAMERA_SERVER_STEREO_PIPE):
            resp["voxl-camera-server"]["mpaStereo"] = VOXL_CAMERA_SERVER_STEREO_PIPE
        else:
            result = False

        resp["voxl-camera-server"]["result"] = result

def voxl_vision_hub_test(resp):
    svc = "voxl-vision-hub"
    result = True

    #
    # Platform checks
    #

    #
    # SDK checks
    #
    if is_service_running(svc):
        resp[svc]["running"] = True
    else:
        result = False

    resp[svc]["result"] = result

def voxl_feature_tracker_test(resp):
    svc = "voxl-feature-tracker"
    result = True

    #
    # Platform checks
    #

    #
    # SDK checks
    #
    if is_service_running(svc):
        resp[svc]["running"] = True
    else:
        result = False

    VOXL_FEATURE_TRACKER_TRACKED_FEATS="/run/mpa/tracked_feats/info"
    if path.exists(VOXL_FEATURE_TRACKER_TRACKED_FEATS):
        resp["voxl-feature-tracker"]["mpaPipe"] = VOXL_FEATURE_TRACKER_TRACKED_FEATS
    else:
        result = False

    resp[svc]["result"] = result


def voxl_mavlink_server_test(resp):
    svc = "voxl-mavlink-server"
    result = True

    #
    # Platform checks
    #

    #
    # SDK checks
    #
    if is_service_running(svc):
        resp[svc]["running"] = True
    else:
        result = False

    resp[svc]["result"] = result


def voxl_flow_server_test(resp):
    svc = "voxl-flow-server"
    result = True

    #
    # Platform checks
    #

    #
    # SDK checks
    #
    if is_service_running(svc):
        resp[svc]["running"] = True
    else:
        result = False

    VOXL_FLOW_SERVER_POS="/run/mpa/flow_pos/info"
    if path.exists(VOXL_FLOW_SERVER_POS):
        resp["voxl-flow-server"]["mpaPipe"] = VOXL_FLOW_SERVER_POS
    else:
        result = False

    resp[svc]["result"] = result

def factory_test_d0008(resp):
    overall_result = True

    # get dmesg output
    p = subprocess.Popen(['dmesg'], stdout=subprocess.PIPE)
    output, err = p.communicate()
    output_str = output.decode('utf-8')
    dmesg_lines = output_str.split('\n')

    resp["uid"] = subprocess.check_output("cat /sys/devices/soc0/serial_number".split()).decode("utf-8").strip()

    voxl_px4_factory_test(resp, "D0008")
    voxl_camera_server_factory_test(resp, "D0008", dmesg_lines)
    voxl_flir_server_test(resp)
    voxl_vision_hub_test(resp)
    voxl_feature_tracker_test(resp)
    voxl_mavlink_server_test(resp)
    voxl_flow_server_test(resp)

    return overall_result


if __name__ == "__main__":

    resp = {}
    resp["factoryMode"] = "NA"
    resp["factoryTestResult"] = "false"

    res, factory_mode = read_file_line(FACTORY_MODE)
    if res:
         resp["factoryTestResult"] = "false"
         
         if "D0008" in factory_mode:
            resp = _d0008_model
            resp["factoryMode"] = factory_mode

            if factory_test_d0008(resp):
                resp["factoryTestResult"] = "true"

    resp_json = json.dumps(resp)
    print(resp_json)
